//
//  RWCardRegion.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <Foundation/Foundation.h>


@class Card;

@protocol RWCardRegion <NSObject>

@property (strong, nonatomic) NSString *name;

- (BOOL)shouldAddCard:(Card *)card fromRegion:(id<RWCardRegion>)sourceRegion;
- (void)addCard:(Card *)card fromRegion:(id<RWCardRegion>)sourceRegion;
- (void)removeCard:(Card *)card;
- (BOOL)containsCard:(Card *)card;


@end
