//
//  RWPlayerTracker.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class Player;
@interface RWPlayerTracker : SKSpriteNode

- (void)doDamage:(NSInteger)damage toPlayer:(Player *)player;
- (NSInteger)healthForPlayer:(Player *)player;

@end
