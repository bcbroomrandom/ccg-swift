//
//  MultilineLabelNode.swift
//  CCG
//
//  Created by Brian Broom on 7/6/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

class MultilineLabelNode : SKSpriteNode {
    
    var fontColor :SKColor = SKColor.whiteColor() {
        didSet {
            retexture()
        }
    }
    
    var fontName :String {
        didSet {
            retexture()
        }
    }
    
    var fontSize :CGFloat = 32.0 {
        didSet {
            retexture()
        }
    }
    var horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
    
    var verticalAlignmentMode :SKLabelVerticalAlignmentMode = SKLabelVerticalAlignmentMode.Baseline {
        didSet {
            retexture()
        }
    }
    
    var text :String = "Label" {
        didSet {
            retexture()
        }
    }
    
    class func labelNode(#fontNamed: String) -> MultilineLabelNode {
        return MultilineLabelNode(fontNamed: fontNamed)
    }
    
    init(fontNamed:String) {
        fontName = fontNamed
        super.init(texture: nil, color: nil, size:CGSizeMake(100, 50))
        retexture()
    }
    
    convenience init() {
        self.init(fontNamed: "Helvetica")
    }
    
    func retexture() {
        let newTextImage = image(fromText: text)
        let newTexture = SKTexture(image: newTextImage)
        self.texture = newTexture
        self.anchorPoint = CGPointMake(0.5, 0.5)
    }
    
    func image(#fromText: String) ->  UIImage {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.ByWordWrapping
        paragraphStyle.alignment = mapSkLabelHorizontalAlignmentToNSTextAlignment(horizontalAlignmentMode)
        paragraphStyle.lineSpacing = 1
        
        let font = UIFont(name: fontName, size:fontSize)
        
        let textAttributes = [NSFontAttributeName: font,
            NSParagraphStyleAttributeName: paragraphStyle,
            NSForegroundColorAttributeName: fontColor]
        
        var textRect = text.bridgeToObjectiveC().boundingRectWithSize(
            UIScreen.mainScreen().bounds.size,
            options: NSStringDrawingOptions.UsesLineFragmentOrigin ,
            attributes: textAttributes,
            context: nil)
        
        // Bug as of Beta2 - NSStringDrawingOptions is defined as :Int
        // the | operator doesn't work correctly with this type
        // dropped one of the options to get it to compile, baselines may be slightly off
        // TODO: Check with Beta 3
        // http://stackoverflow.com/questions/24064650/how-to-pass-multiple-enum-values-as-a-function-parameter
        //        let textRect = text.bridgeToObjectiveC().boundingRectWithSize(
        //            UIScreen.mainScreen().bounds.size,
        //            options: NSStringDrawingOptions.UsesLineFragmentOrigin | NSStringDrawingOptions.TruncatesLastVisibleLine,
        //            attributes: textAttributes,
        //            context: nil)
        
        // sizes are fractional, round them
        textRect.size.height = ceil(textRect.size.height)
        textRect.size.width = ceil(textRect.size.width)
        
        // set sprite size as bounding rect for text
        size = textRect.size
        
        // generate the image. Create context, draw text, create image, close context.
        UIGraphicsBeginImageContextWithOptions(textRect.size, false, 0.0)
        text.bridgeToObjectiveC().drawInRect(textRect, withAttributes: textAttributes)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func mapSkLabelHorizontalAlignmentToNSTextAlignment(alignment: SKLabelHorizontalAlignmentMode) -> NSTextAlignment {
        switch alignment {
        case SKLabelHorizontalAlignmentMode.Left:
            return NSTextAlignment.Left
        case SKLabelHorizontalAlignmentMode.Center:
            return NSTextAlignment.Center
        case SKLabelHorizontalAlignmentMode.Right:
            return NSTextAlignment.Right
        default:
            return NSTextAlignment.Left
        }
    }
}