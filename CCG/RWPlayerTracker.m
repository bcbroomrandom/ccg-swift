//
//  RWPlayerTracker.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWPlayerTracker.h"
#import "UIColor+CCGColors.h"
#import "RWGameScene.h"
//#import "RWPlayer.h"
#import "RW_CCG-Swift.h"


@interface RWPlayerTracker ()

@property (weak, nonatomic, readonly) RWGameScene *gameScene;

@property (assign, nonatomic) NSInteger p1Health;
@property (assign, nonatomic) NSInteger p2Health;
@property (strong, nonatomic) SKLabelNode *p1HealthLabel;
@property (strong, nonatomic) SKLabelNode *p2HealthLabel;
@property (strong, nonatomic) SKSpriteNode *p1Indicator;
@property (strong, nonatomic) SKSpriteNode *p2Indicator;

@property (strong, nonatomic) SKSpriteNode *endTurnIndicator;

@property (assign, nonatomic) NSInteger playerNumber;


@end

@implementation RWPlayerTracker

- (id)init
{
    self = [super initWithColor:[UIColor clearColor] size:CGSizeMake(150, 360)];
    if (self) {
        
        // player one
        _p1Indicator = [SKSpriteNode spriteNodeWithImageNamed:@"player1_druid.png"];
        _p1Indicator.position = CGPointMake(0, -99);
        [self addChild:_p1Indicator];
        
        self.p1Health = 20;
        SKSpriteNode *p1Heart = [SKSpriteNode spriteNodeWithImageNamed:@"icon_heart.png"];
        p1Heart.position = CGPointMake(-25, -150);
        [self addChild:p1Heart];
        
        _p1HealthLabel = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
        _p1HealthLabel.text = [NSString stringWithFormat:@"%ld", (long)self.p1Health];
        _p1HealthLabel.fontSize = 36;
        _p1HealthLabel.fontColor = [UIColor blackColor];
        _p1HealthLabel.position = CGPointMake(20, -165);
        [self addChild:_p1HealthLabel];
        
        //player two
        _p2Indicator = [SKSpriteNode spriteNodeWithImageNamed:@"player2_beastmaster.png"];
        _p2Indicator.position = CGPointMake(0, 99);
        _p2Indicator.zRotation = M_PI;
        [self addChild:_p2Indicator];
        
        self.p2Health = 20;
        SKSpriteNode *p2Heart = [SKSpriteNode spriteNodeWithImageNamed:@"icon_heart.png"];
        p2Heart.position = CGPointMake(25, 150);
        p2Heart.zRotation = M_PI;
        [self addChild:p2Heart];
        
        _p2HealthLabel = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
        _p2HealthLabel.text = [NSString stringWithFormat:@"%ld", (long)self.p2Health];
        _p2HealthLabel.fontSize = 36;
        _p2HealthLabel.fontColor = [UIColor blackColor];
        _p2HealthLabel.position = CGPointMake(-20, 165);
        _p2HealthLabel.zRotation = M_PI;
        [self addChild:_p2HealthLabel];
        
        // phase labels
        
        _endTurnIndicator = [SKSpriteNode spriteNodeWithImageNamed:@"end_turn.png"];
        _endTurnIndicator.position = CGPointMake(0, 0);
        [self addChild:_endTurnIndicator];
        
        _playerNumber = 1;
    }
    return self;
}

- (RWGameScene *)gameScene {
    return (RWGameScene *)self.scene;
}

- (void)advanceTurn {
    
    // which sound to run
    [self runAction:[SKAction playSoundFileNamed:@"clank.wav" waitForCompletion:NO]];
    //[self runAction:[SKAction playSoundFileNamed:@"clink.wav" waitForCompletion:NO]];
    
    if (self.playerNumber == 1) {
        self.playerNumber = 2;
        
        self.endTurnIndicator.zRotation = M_PI;
        
    } else {
        self.playerNumber = 1;
        
        self.endTurnIndicator.zRotation = 0;
    }
    
    [self.gameScene nextTurn];
}

- (NSInteger)healthForPlayer:(Player *)player {
    if ([player.name isEqualToString:@"Druid"]) {
        return self.p1Health;
    } else {
        return self.p2Health;
    }
}

- (void)doDamage:(NSInteger)damage toPlayer:(Player *)player {
    if ([player.name isEqualToString:@"Druid"]) {
        self.p1Health -= damage;
        //if (self.p1Health < 0) { self.p1Health = 0; }
        self.p1HealthLabel.text = [NSString stringWithFormat:@"%ld", (long)self.p1Health];
        if (self.p1Health <= 0) { [self.gameScene playerDies:player]; }
    }
    
    if ([player.name isEqualToString:@"Beastmaster"]) {
        self.p2Health -= damage;
        if (self.p2Health < 0) { self.p2Health = 0; }
        self.p2HealthLabel.text = [NSString stringWithFormat:@"%ld", (long)self.p2Health];
        if (self.p2Health <= 0) { [self.gameScene playerDies:player]; }
    }
}

#pragma mark - Touch Handlers
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    if ([self.endTurnIndicator containsPoint:location]) {
        [self advanceTurn];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}



@end
