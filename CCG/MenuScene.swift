//
//  MenuScene.swift
//  CCG
//
//  Created by Brian Broom on 7/4/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import AVFoundation
import SpriteKit

class MenuScene : SKScene {
    
    let musicPlayer :AVAudioPlayer
    
    init(size: CGSize) {
        var error :NSError? = nil
        let path = NSBundle.mainBundle().pathForResource("Take a Chance", ofType: "mp3")
        let musicFile = NSURL(fileURLWithPath: path)
        musicPlayer = AVAudioPlayer(contentsOfURL: musicFile, error: &error)
        
        super.init(size: size)
        
        backgroundColor = SKColor(red: 0.15, green: 0.15, blue: 0.3, alpha: 1.0)
        
        let bg = SKSpriteNode(imageNamed: "MenuBackground.png")
        bg.position = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame))
        addChild(bg)
        
        let playButton = SKSpriteNode(imageNamed: "PlayButton.png")
        playButton.position = CGPointMake(650, 330)
        playButton.name = "play"
        addChild(playButton)
        
        let tutorialButton = SKSpriteNode(imageNamed: "TutorialButton.png")
        tutorialButton.position = CGPointMake(650, 450)
        tutorialButton.name = "tutorial"
        addChild(tutorialButton)
        
        let creditsButton = SKSpriteNode(imageNamed: "CreditsButton.png")
        creditsButton.position = CGPointMake(650, 210)
        creditsButton.name = "credits"
        addChild(creditsButton)
        
        let linkButton = SKSpriteNode(imageNamed: "LinkButton.png")
        linkButton.position = CGPointMake(CGRectGetMidX(frame), 70)
        linkButton.name = "link"
        addChild(linkButton)
        
        musicPlayer.prepareToPlay()
        musicPlayer.play()
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        let touch = touches.anyObject() as UITouch
        let location = touch.locationInNode(self)
        let touchNode = nodeAtPoint(location)
        
        let pushDown = SKAction.scaleTo(0.8, duration: 0.2)
        let click = SKAction.playSoundFileNamed("click.wav", waitForCompletion: true)
        let pushUp = SKAction.scaleTo(1.0, duration: 0.2)
        
        let buttonPress = SKAction.sequence([pushDown, click, pushUp])
        
        if !(touchNode.name) { return }
        
        switch touchNode.name! {
            
        case "link" :
            touchNode.runAction(buttonPress) {
                self.musicPlayer.stop()
                let url = NSURL(string: "http://www.raywenderlich.com/")
                UIApplication.sharedApplication().openURL(url)
            }
            
        case "tutorial" :
            touchNode.runAction(buttonPress) {
                self.musicPlayer.stop()
                let tutorialScene = TutorialScene(size: self.size)
                self.view .presentScene(tutorialScene)
            }
            
        case "play" :
            touchNode.runAction(buttonPress) {
                self.musicPlayer.stop()
                let gameScene = RWGameScene(size: self.size)
                self.view .presentScene(gameScene)
            }
            
        case "credits" :
            touchNode.runAction(buttonPress) {
                self.musicPlayer.stop()
                let creditScene = CreditScene(size: self.size)
                self.view .presentScene(creditScene)
            }
            
        default:
            return
        }
    }
    
}