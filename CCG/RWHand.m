//
//  RWHand.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWHand.h"
#import "UIColor+CCGColors.h"
#import "RW CCG-Bridging-Header.h"
#import "RW_CCG-Swift.h"

@interface RWHand ()

@property (strong, nonatomic) NSMutableArray *cards;

@end

@implementation RWHand

- (instancetype)init {
    self = [super initWithColor:[UIColor clearColor] size:CGSizeMake(640, 160)];
    if (self) {
        _cards = [NSMutableArray new];
        self.name = @"Hand";
    }
    return self;
}

#pragma mark - Hand Actions

- (void)faceUp {
    for (Card *card in self.cards) {
        [card faceUp];
    }
}

- (void)faceDown {
    for (Card *card in self.cards) {
        [card faceDown];
    }
}

#pragma mark - RWCardRegion protocol methods

- (BOOL)shouldAddCard:(Card *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    return NO;
}

- (void)addCard:(Card *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    [self.cards addObject:card];
    card.currentRegion = self;
    CGPoint newPosition = [self positionForCard:card];
    
    SKAction *move = [SKAction moveTo:newPosition duration:0.3];
    [card runAction:move];
    
    if (sourceRegion && [sourceRegion containsCard:card]) {
        [sourceRegion removeCard:card];
    }
}

- (BOOL)containsCard:(Card *)card {
    return [self.cards containsObject:card];
}

- (void)removeCard:(Card *)card {
    [self.cards removeObject:card];
    [self recalculateCardPositions];
}

- (BOOL)canAttackCardAttackAtPoint:(CGPoint)point {
    return NO;
}

#pragma mark - Card Position Calculations

- (CGPoint)positionForCard:(Card *)card {
    NSInteger index = [self.cards indexOfObject:card];
    CGFloat xPos = (2 * index + 1) * card.size.width / 2;
    CGFloat buffer = index * 4;
    return CGPointMake( CGRectGetMinX(self.frame) + xPos + buffer, self.position.y);
}

- (void)recalculateCardPositions {
    for (Card* card in self.cards) {
        card.position = [self positionForCard:card];
    }
}

@end
