//
//  RWInPlayRegion.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWInPlayRegion.h"
#import "UIColor+CCGColors.h"
#import "RWGameScene.h"
#import "RWStarTracker.h"
#import "RW CCG-Bridging-Header.h"
#import "RW_CCG-Swift.h"

@interface RWInPlayRegion ()

@property (strong, nonatomic) NSMutableArray *cards;
@property (weak, nonatomic, readonly) RWGameScene *gameScene;

@end

@implementation RWInPlayRegion

- (id)init {
    self = [super initWithColor:[UIColor clearColor] size:CGSizeMake(590, 204)];
    if (self) {
        _cards = [NSMutableArray new];
    }
    return self;
}

- (RWGameScene *)gameScene {
    return (RWGameScene *)self.scene;
}

#pragma mark - Attack Phase methods

- (NSArray *)creatures {
    NSPredicate *creatureFilter = [NSPredicate predicateWithFormat:@"(cardType = 'Creature')"];
    return [self.cards filteredArrayUsingPredicate:creatureFilter];

}

- (NSArray *)fighters {
    NSPredicate *fighterFilter = [NSPredicate predicateWithFormat:@"(cardType = 'Creature') AND (attackedThisTurn = NO) AND (isIncapacitated = NO)"];
    return [self.cards filteredArrayUsingPredicate:fighterFilter];
}

- (void)clearHighlights {
    for (Card *card in self.cards) {
        [card clearHighlight];
    }
}

- (void)resetAttacks {
    for (Card *card in self.cards) {
        card.attackedThisTurn = NO;
    }
}

- (void)checkCreaturesForExpiredAttachments {
    for (Card *creature in self.creatures) {
        [creature haveAttachmentsExpired];
    }
}

#pragma mark - Card Positions

- (void)layoutCards {
    for (Card *card in self.cards) {
        card.position = [self positionForCard:card];
        [card restackAttachments];
    }
}

- (CGPoint)positionForCard:(Card *)card {
    NSInteger index = [self.cards indexOfObject:card];
    //NSLog(@"%f", self.position.x - self.frame.size.width/2 + card.frame.size.width/2 + 10);
    return CGPointMake(325 + 110 * index, self.position.y);
}

#pragma mark - RWCardRegion Protocol Methods

- (BOOL)shouldAddCard:(Card *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    
    if (card.energyCost > self.gameScene.starTracker.stars){
        [self.gameScene notifyWithString:@"Activate more stars to cast."];
        return NO;
    }
    
    Card *cardDroppedOn = [self cardAtPoint:card.position];
    if ([card.cardType isEqualToString:@"Spell"] && cardDroppedOn &&
        ((card.player == cardDroppedOn.player && [card.spellTargets isEqualToString:@"Friendly"]) ||
         (card.player != cardDroppedOn.player && [card.spellTargets isEqualToString:@"Enemy"]) )) {
            //NSLog(@"cast %d on %d", card.cardName, cardDroppedOn.cardName);
            //[self.gameScene cast:card onTarget:cardDroppedOn];
            return YES;
        }
    
    // if a spell card gets this far, it was not dragged onto the propper creature
    // and should not be cast
    if ([card.cardType isEqualToString:@"Spell"]) {
        if ([card.spellTargets isEqualToString:@"Friendly"]) {
            [self.gameScene notifyWithString:@"Drag onto one of your creatures to cast."];
        } else {
            [self.gameScene notifyWithString:@"Drag onto an enemy creature to cast."];
        }
        return NO;
    }
    
    if (card.player != self.player) {
        [self.gameScene notifyWithString:@"Play your cards in your half of the table."];
        return NO;
    }
    
    if ([card.cardType isEqualToString:@"Energy"] ) {
        [self.gameScene notifyWithString:@"Energy cards go in the Energy region."];
        return NO;
    }
    
    return YES;
    
}

- (void)addCard:(Card *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    [self.cards addObject:card];
    [self.gameScene.starTracker removeStars:card.energyCost];
    [sourceRegion removeCard:card];
    card.currentRegion = self;
    
    [card runAction:[SKAction moveTo:[self positionForCard:card] duration:0.3]];
    
    if ([card.cardType isEqualToString:@"Spell"]) { [self.gameScene selectTargetsForSpell:card]; }
}

- (BOOL)containsCard:(Card *)card {
    return [self.cards containsObject:card];
}

- (void)removeCard:(Card *)card {
    if ([self containsCard:card]) {
        [self.cards removeObject:card];
    }
    [self layoutCards];
}

- (Card *)cardAtPoint:(CGPoint)point {
    Card *matchCard;
    for (Card *card in self.cards) {
        if ([card containsPoint:point]) {
            matchCard = card;
        }
    }
    return matchCard;
}

//- (BOOL)canAttackAtPoint:(CGPoint)point {
//    BOOL match = NO;
//    for (RWCard *card in self.cards) {
//        if ([card containsPoint:point] &&
//            [card.cardType isEqualToString:@"Creature"] &&
//            (card.player != self.gameScene.currentPlayer)) {
//            match = YES;
//        }
//    }
//    return match;
//}

- (void)attackCardAtPoint:(CGPoint)point withCard:(Card *)attacker {
    Card *cardMatch;
    for (Card *card in self.cards) {
        if ([card containsPoint:point] &&
            [card.cardType isEqualToString:@"Creature"] &&
            (card.player != self.gameScene.currentPlayer)) {
            cardMatch = card;
        }
    }
    [self.gameScene fightWithAttacker:attacker andDefender:cardMatch];
}

@end
