//
//  Player.swift
//  CCG
//
//  Created by Brian Broom on 7/8/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

class Player : NSObject {
    var name :String?
    var rotation :CGFloat = 0
    var active :Bool = false
    var turnNotifyOverlay :SKNode?
    var deck :Deck?
    var discard :RWDiscard?
    var hand :RWHand?
    var energyTracker :RWEnergyTracker?
    var inPlay :RWInPlayRegion?
    
    // default init
    init() {
        
    }
    
    func makeActive() {
        active = true
        if deck {
            deck!.active = true
        }
        if energyTracker {
            energyTracker!.hasPlayedEnergyCardThisTurn = false
        }
    }
    
    func makeInactive() {
        active = false
        if deck {
            deck!.active = false
            
        }
    }
}