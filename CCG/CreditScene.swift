//
//  CreditScene.swift
//  CCG
//
//  Created by Brian Broom on 7/6/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

class CreditScene : SKScene {
    
    let musicPlayer :AVAudioPlayer
    var counter = 0
    let messageAndLink :Array<(message: String, link: String)> = []
    let label :MultilineLabelNode
    
    init(size: CGSize) {
        // sound setup
        // play background music
        let path = NSBundle.mainBundle().pathForResource("Vulcan", ofType: "mp3")
        let musicFile = NSURL(fileURLWithPath: path)
        
        var error :NSError? = nil
        musicPlayer = AVAudioPlayer(contentsOfURL: musicFile, error: &error)
        musicPlayer.numberOfLoops = -1
        musicPlayer.prepareToPlay()
        musicPlayer.play()
        
        messageAndLink += ("Special thanks to", "")
        messageAndLink += ("Artwork by Vicki Wenderlich", "http://www.vickiwenderlich.com")
        messageAndLink += ("Incompetech.com for music", "http://incompetech.com/")
        messageAndLink += ("Freesound.org for sound effects", "http://www.freesound.org/")
        
        messageAndLink += ("\"Black Vortex\" Kevin MacLeod (incompetech.com) " +
            "Licensed under Creative Commons: By Attribution 3.0 " +
            "http://creativecommons.org/licenses/by/3.0/",
            "http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1300037")
        
        messageAndLink += ("\"Take a Chance\" Kevin MacLeod (incompetech.com) " +
            "Licensed under Creative Commons: By Attribution 3.0 " +
            "http://creativecommons.org/licenses/by/3.0/",
            "http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1300024")
        
        messageAndLink += ("\"Pippin the Hunchback\" Kevin MacLeod (incompetech.com) " +
            "Licensed under Creative Commons: By Attribution 3.0 " +
            "http://creativecommons.org/licenses/by/3.0/",
            "http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1400005")
        
        messageAndLink += ("\"Vulcan\" Kevin MacLeod (incompetech.com) " +
            "Licensed under Creative Commons: By Attribution 3.0 " +
            "http://creativecommons.org/licenses/by/3.0/",
            "http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1100525")
        
        messageAndLink += ("Click sound by TicTacShutUp", "http://www.freesound.org/people/TicTacShutUp/")
        
        messageAndLink += ("Card draw sound by gynation",
            "http://www.freesound.org/people/gynation/sounds/82379/")
        
        messageAndLink += ("Card flip sound by Koops",
            "http://www.freesound.org/people/Koops/sounds/20241/")
        
        messageAndLink += ("Sword Clang 1 by Erdie", "http://www.freesound.org/people/Erdie/sounds/27826/")
        
        messageAndLink += ("Sword Clang 2 by Erdie", "http://www.freesound.org/people/Erdie/sounds/27858/")
        
        messageAndLink += ("Creature Death Sound by numar", "http://www.freesound.org/people/numar/sounds/118401/")
        
        messageAndLink += ("Victory Sound by primordiality",
            "http://www.freesound.org/people/primordiality/sounds/78824/")
        
        messageAndLink += ("Phase change clink by DJ Burnham",
            "http://www.freesound.org/people/DJ%20Burnham/sounds/76812/")
        
        messageAndLink += ("Turn Change clink by Agonda",
            "http://www.freesound.org/people/Agonda/sounds/210581/")
        
        messageAndLink += ("Death Ray zap by themfish", "http://www.freesound.org/people/themfish/sounds/34205/")
        
        messageAndLink += ("Energy Activation by fins",
            "http://www.freesound.org/people/fins/sounds/191592/")
        
        messageAndLink += ("Open Sans font from Google Fonts", "http://www.google.com/fonts/specimen/Open+Sans")
        
        messageAndLink += ("Onramp font by Michael Spitz", "http://www.losttype.com/font/?name=ONRAMP,")
        
        messageAndLink += ("Text overlay code uses DSMultilineLabelNode by Chris Allwein",
            "https://github.com/downrightsimple/DSMultilineLabelNode")

        
        // message node
        label = MultilineLabelNode(fontNamed: "OpenSans")
        
        super.init(size: size)
        
        // background image
        let bg = SKSpriteNode(imageNamed: "bg_blank.png")
        bg.position = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame))
        addChild(bg)
        
        // setup label
        label.fontSize = 40;
        label.text = messageAndLink[counter].message;
        label.position = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame))
        label.alpha = 0
        addChild(label)
        
        let fadeIn = SKAction.fadeInWithDuration(0.5)
        let delay = SKAction.waitForDuration(2.0)
        let fadeOut = SKAction.fadeOutWithDuration(0.5)
        let nextMessage = SKAction.runBlock(nextCredit)
        
        let cycle = SKAction.sequence([fadeIn, delay, fadeOut, nextMessage])
        let repeatCycle = SKAction.repeatActionForever(cycle)
        
        label.runAction(repeatCycle)

        // button to return to menu
        let menuButton = SKSpriteNode(imageNamed: "MenuButton.png")
        menuButton.name = "menu"
        menuButton.position = CGPointMake(CGRectGetMidX(frame), 50)
        addChild(menuButton)
        
    }
    
    func nextCredit() {
        counter++
        if counter == messageAndLink.count {
            counter = 0
        }
        
        label.text = messageAndLink[counter].message
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        let touch = touches.anyObject() as UITouch
        let location = touch.locationInNode(self)
        let touchNode = nodeAtPoint(location)
        
        let pushDown = SKAction.scaleTo(0.8, duration: 0.2)
        let click = SKAction.playSoundFileNamed("click.wav", waitForCompletion: true)
        let pushUp = SKAction.scaleTo(1.0, duration: 0.2)
        
        let clickAndUp = SKAction.group([click, pushUp])
        let push = SKAction.sequence([pushDown, clickAndUp])
        
        if touchNode.name == "menu" {
            touchNode.runAction(push) {
                self.musicPlayer.stop()
                let menuScene = MenuScene(size: self.size)
                self.view.presentScene(menuScene)
            }
        }
    }
}
